<?php

$chunks = array();

$list = array('ims_form','ims_header');

foreach($list as $v){

  $chunk_name = $v;
  $content = getSnippetContent($sources['chunks'] . $chunk_name . '.chunk.tpl');
  if(!empty($content)){
    $chunk = $modx->newObject('modChunk', array(
     'name'          => $chunk_name,
     'description'   => $chunk_name.'_desc',
     'snippet'       => $content,
    ));
    $chunks[] = $chunk;
  }
  
}

unset($chunk,$chunk_name,$content);
return $chunks;