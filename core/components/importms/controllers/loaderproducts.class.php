<?php

if(!defined('MODX_API_MODE')){
  define('MODX_API_MODE', true);
}

class loaderProducts{

  function __construct(&$modx, $config){ 
    $this->modx = $modx;
    // Load main services
    $this->modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
    $this->modx->setLogLevel($is_debug ? modX::LOG_LEVEL_INFO : modX::LOG_LEVEL_ERROR);
    $this->modx->getService('error','error.modError');
    $this->modx->lexicon->load('minishop2:default');
    $this->modx->lexicon->load('minishop2:manager');


    $this->config = array_merge(array(
        "directoryimage" => "",
        "load_all_category" => "",
        "fields" => "",
        "category" => 0,
        "operation" => 0,
        "key" => "pagetitle",
        "is_debug" => 0,
        "delimiter" => '|'
    ),$config);
  }

  function isUTF8($str){
    if(mb_detect_encoding($str,"UTF-8",true)!==false)
      return true;
    else
      return false;
  }


  function csv_import(){


    $info = array();

    // Load main services
    $this->modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
    $this->modx->setLogLevel($is_debug ? modX::LOG_LEVEL_INFO : modX::LOG_LEVEL_ERROR);
    $this->modx->getService('error','error.modError');
    $this->modx->lexicon->load('minishop2:default');
    $this->modx->lexicon->load('minishop2:manager');
/*
    // Time limit
    set_time_limit(600);
    $tmp = 'Установка максимального времени выполнения скрипта = 600 sec: ';
    $tmp .= ini_get('max_execution_time') == 600 ? 'успешно' : 'ошибка';
    $info[] = array('type_message' => 'warning', 'message' => $tmp);
*/
    // Check file
    if (empty($this->config['file']['name'])) {
      $error = 'Выберите файл для импорта';
      $info[] = array('type_message' => 'error', 'message' => $error);
      return $info;
    }
    elseif (!preg_match('/\.csv$/i', $this->config['file']['name'])) {
      $info[] = array('type_message' => 'error', 'message' => "Загружаемый файл должен быть формата csv");
      return $info;
    }

    // Проверяем загружен ли файл
    if(!is_uploaded_file($this->config['file']['tmp_name']))
    {
      $info[] = array('type_message' => 'error', 'message' => "Ошибка загрузки файла для импорта");
      return $info;
    }

    //проверка директории изображений
    if (!is_dir(MODX_BASE_PATH.$this->config['directoryimage'])) {
      $info[] = array('type_message' => 'error', 'message' => "Не найдена директория для изображений товаров на сервере: ".$this->config['directoryimage']);
      return $info;        
    }

    // Check required options
    if (empty($this->config['fields'])) {
      $info[] = array('type_message' => 'error', 'message' => "Вы должны указать порядок полей в файле");
      return $info;
    }

    if (empty($this->config['key'])) {
      $info[] = array('type_message' => 'error', 'message' => "Вы должны указать key-поле. Используется для определения совпадения дубликатов");
      return $info;
    }

    $keys = array_map('trim', explode(',', strtolower($this->config['fields'])));
    $tv_enabled = false;
    foreach ($keys as $v) {
      if (preg_match('/^tv(\d)$/', $v)) {
        $tv_enabled = true;
        break;
      }
    }

    if (empty($this->config['delimiter'])) {
     $this->config['delimiter'] = ';';
     $info[] = array('type_message' => 'warning', 'message' => "По умолчанию назначен разделитель полей (;)");
    }


    // Import!
    $handle = fopen($this->config['file']['tmp_name'], "r");
    $rows = $created = $updated = 0;
    while (($csv = fgetcsv($handle, 0, $this->config['delimiter'])) !== false) {

    //переводим поля в нужную кодировку
      foreach ($csv as $key => $value) {
         $csv[$key] = $this->isUTF8($value) ? $csv[$key] : iconv('cp1251','UTF-8',$value); 
      }

      $rows ++;
      $data = $gallery = array();
      $this->modx->error->reset();

      foreach ($keys as $k => $v) {

        if (!isset($csv[$k])) {
          $info[] = array('type_message' => 'error', 'message' => 'Поле "' . $v . '" не найдено в файле. Проверьте файл для импорта или поле для перечисления полей.');
          return $info;
        }
        if ($v == 'gallery') {
          $gallery[] = $csv[$k];
        }
        elseif (isset($data[$v]) && !is_array($data[$v])) {
          $data[$v] = array($data[$v], $csv[$k]);
        }
        elseif (isset($data[$v]) && is_array($data[$v])) {
          $data[$v][] = $csv[$k];
        }
        else {
          $data[$v] = $csv[$k];
        }
      }

      $is_product = false;

      // Set default values
      if  (($this->config['load_all_category'] == 1)  && isset($this->config['category'])) 
        $data['parent'] =  $this->config['category'];

      if (empty($data['class_key'])) {$data['class_key'] = 'msProduct';}
      if (empty($data['context_key'])) {
        if (isset($data['parent']) && $parent = $this->modx->getObject('modResource', $data['parent'])) {
          $data['context_key'] = $parent->get('context_key');
        }
        elseif (isset($this->modx->resource) && isset($this->modx->context)) {
          $data['context_key'] = $this->modx->context->key;
        }
        else {
          $data['context_key'] = 'web';
        }
      }
      $data['tvs'] = $tv_enabled;

      // Duplicate check
      $q = $this->modx->newQuery($data['class_key']);
      $q->select($data['class_key'].'.id');
      if (strtolower($data['class_key']) == 'msproduct') {
        $q->innerJoin('msProductData', 'Data', $data['class_key'].'.id = Data.id');
        $is_product = true;
      }
      $tmp = $this->modx->getFields($data['class_key']);
      if (isset($tmp[$this->config['key']])) {
        $q->where(array($this->config['key'] => $data[$this->config['key']]));
      }
      elseif ($is_product) {
        $q->where(array('Data.'.$this->config['key'] => $data[$this->config['key']]));
      }
      $q->prepare();
                
      /** @var modResource $exists */
      if ($exists = $this->modx->getObject($data['class_key'], $q)) {
                  
        if (!$this->config['operation']) {
            $info[] = array('type_message' => 'warning', 'message' => "(Стр ".$rows.")"." Совпадение объектов. Пропуск строки с ".$this->config['key']."=".$data[$this->config['key']]." товар не обновлен!");

          if ($is_debug) {
            $this->modx->log(modX::LOG_LEVEL_INFO, 'You in debug mode, so we process only 1 row. Time: '.number_format(microtime(true) - $this->modx->startTime, 7) . " s");
            exit;
          }
          else {continue;}
        }
        else {
          $action = 'update';
          $data['id'] = $exists->id;
        }
      }
      else {
        $action = 'create';
      }

      $data['published'] = '1'; //опубликовать товар

      // Create or update resource
      /** @var modProcessorResponse $response */
      $response = $this->modx->runProcessor('resource/'.$action, $data);
      if ($response->isError()) {
        $info[] = array('type_message' => 'error', 'message' => "(Стр ".$rows.")"." Error on $action: \n". print_r($response->getAllErrors(), 1));
      }
      else {
        if ($action == 'update') {$updated ++;}
        else {$created ++;}

        $resource = $response->getObject();
          
        // Process gallery images, if exists
        if (!empty($gallery)) {
          
          foreach ($gallery as $v) {
            if (empty($v)) {continue;}
            $image = str_replace('//', '/', MODX_BASE_PATH.$this->config['directoryimage'].$v);
           

            if (!file_exists($image)) {
             $info[] = array('type_message' => 'error', 'message' => "(Стр ".$rows.")"." Ошибка импорта изображения \"$v\" в  галерею. Файл ".$this->config['directoryimage'].$v." не
    найден на сервере.");
            }
            else {
              $response = $this->modx->runProcessor('gallery/upload',
                array('id' => $resource['id'], 'name' => $v, 'file' => $image),
                array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
              );
              if ($response->isError()) {
                $info[] = array('type_message' => 'error', 'message' => "(Стр ".$rows.")"." Ошибка загрузки \"$v\":".print_r($response->getAllErrors(), 1));
              }
              else {
                //$info[] = array('type_message' => 'info', 'message' => "Изображение загружено на сервер: \"$v\"");
              }
            }
          }
        }
      }

      if ($is_debug) {
        $this->modx->log(modX::LOG_LEVEL_INFO, 'You in debug mode, so we process only 1 row. Time: '.number_format(microtime(true) - $this->modx->startTime, 7) . " s");
        exit;
      }
    }
    fclose($handle);


    $info[] = array('type_message' => 'info', 'message' => "---------------------------");
    $info[] = array('type_message' => 'info', 'message' => "Статистика:");
    $info[] = array('type_message' => 'info', 'message' => "Всего строк: ".$rows);
    $info[] = array('type_message' => 'info', 'message' => "Создано товаров: ".$created);
    $info[] = array('type_message' => 'info', 'message' => "Изменено товаров: ".$updated);

    $info[] = array('type_message' => 'default', 'message' => "Импорт продуктов завершен.");

    return $info;
  }
}

?>


