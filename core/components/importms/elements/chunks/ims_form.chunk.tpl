<div id="inline">
   <form class="form" id="import" name="import" action="#" method="post" enctype="multipart/form-data">
   <h2>Импорт minishop2</h2>
   <br>
   <div class="wrapper">
  <label>Загрузить товар: </label><br>
  <div class="field">
      <input type='radio' name='load_all_category' id='load_all_category' value='0' checked='checked'>По всем категориям<br>
<input type='radio' name='load_all_category' id='load_category' value='1'>В категорию
       <span>[[!ims_category_option]]</span>
  </div>
    </div><br>
    <div class="wrapper">
      <label for="fields">Последовательность полей в файле для импорта (перечисляются через запятую)</label><br>
      <div class="field">
        <textarea id="fields" name="fields" cols="70" class="txt" rows="7" required>parent,pagetitle,article,price,gallery</textarea>
        <span style="font-size: 80%">* Поля обязательные для заполнения: parent,pagetitle,article!</span>
      </div>
    </div><br>
    <div class="wrapper">
      <label for="key">Выберите ключевое поле (для сравнения товаров): </label>
      <span>
        <select name="key" id="key">
        <option value='pagetitle' selected='selected'>pagetitle</option>
        <option value='article'>article</option>
        </select>
      </span>     
    </div><br>
    <div class="wrapper">
      <label for="delimiter">Разделитель полей: </label>
      <span>
        <select name="delimiter" id="delimiter">
          <option value='|'>вертикальная линия (|)</option>
          <option value=';' selected='selected'>точка с запятой (;)</option>
        </select>
      </span>
    </div></br>
    <div class="wrapper">
      <label for="operation">Операция над базой данных: </label>
      <span>
        <input type='radio' name='operation' value='0' checked='checked'>Добавить товары
        <input type='radio' name='operation' value='1'>Обновить товары
      </span>
    </div></br>
    <div class="wrapper">
      <label for="uploaddir">Файл для импорта</label><br>
      <div class="field">
        <input size='50' title="Выбрать и загрузить файл на сервер" type='file' id='loadfile' name='loadfile'/>
      </div>
    </div><br>
    <div class="wrapper">
      <label for="directoryimage"  class="style">Директория хранения изображений на сервере:</label>
      <div class="field">
        <input type="text" name="directoryimage" id="directoryimage" value="assets/files/" size="50" />
      </div>
    </div><br>
    <div class="wrapper">
       <button id="send">Импорт товаров</button>
    </div>
    <div class="wrapper">
      <label for="msg">Сообщения сервера:</label>
       <div id="msg" class="msg"></div>
    </div>
   </form>
</div>