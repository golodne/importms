$(document).ready(function() {

  $("#load_all_category").click(function() {
      $("#category").attr("disabled", true);
  });
  $("#load_category").click(function() {
      $("#category").attr("disabled", false);
  });

  $(".modalbox").fancybox({
    fitToView: false,
    padding: 20
  });

  $("#import").submit(function() { 
      return false; 
  });

  $("#send").on("click", function(){
    var fieldsval  = $("#fields").val();
    var category = $("#category").val();
    var key = $("#key").val();
    var delimiter = $("#delimiter").val();
    var operation = $("input:radio[name ='operation']:checked").val();
    var load_all_category = $("input:radio[name ='load_all_category']:checked").val();
    var uploaddir = $("#uploaddir").val();
    var directoryimage = $("#directoryimage").val();

    //валидация полей
    var mailvalid = true;
    
    //валидация полей
    if(mailvalid == true){

      $("#send").hide();
      $('#msg').empty();

      var file_data = $("#loadfile").prop("files")[0];
      var form_data = new FormData();

      form_data.append('loadfile', file_data);
      form_data.append('load_all_category',load_all_category);
      form_data.append('fields', fieldsval);
      form_data.append('category',category);
      form_data.append('key',key);
      form_data.append('delimiter',delimiter);
      form_data.append('operation',operation);
      form_data.append('uploaddir',uploaddir);
      form_data.append('directoryimage',directoryimage);

      $.ajax({
          type: 'POST',
          url: 'assets/components/importms/connectors/connector.php?action=web/import',
          processData: false,
          contentType: false,
          data: form_data,

          success: function(response) {

            var resonse_decode = JSON.parse(response);
            var status = resonse_decode[0];
            var messages = resonse_decode[1];


            if (status == 1) {
               $('#msg').append($('<p>', { text: "Получен ответ сервера: " }).css({'color' : 'black'}));
               
               $.each(messages, function(i, data) {
                 switch (data.type_message)
                  {
                    case "info": $('#msg').append($('<p>', {text: data.message}).css({'color' : 'blue'})); break;
                    case "warning": $('#msg').append($('<p>', {text: data.message}).css({'color' : 'green'})); break;
                    case "error": $('#msg').append($('<p>', {text: data.message}).css({'color' : 'red'})); break;
                    default: $('#msg').append($('<p>', {text: data.message}).css({'color' : 'black'}));
                  }
                });
            }
            else { $('#msg').append($('<p>', {text: "не распознан ответ сервера."}).css({'color' : 'red'})); }
          }
      }); //end ajax!

    $("#send").show();
    }
  });

});