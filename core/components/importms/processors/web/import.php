<?php


if(!defined('MODX_API_MODE')){
  define('MODX_API_MODE', true);
}


$srv_msg = array(); //стэк сообщений
$status = 1; //ответ во front


//проверка прав пользователя для создания ресурсов на сервере
if (!$modx->hasPermission('new_document')){
  $srv_msg[] = array('type_message' => 'error', 'message' => "Политика доступа: вы не можете создавать ресурсы на сервере.");
  echo json_encode(array($status,$srv_msg));
  return FALSE;  
}

if (!class_exists('loaderProducts')) {
  $srv_msg[] = array('type_message' => 'error', 'message' => "LoaderProducts: не найден файл: ".$loaderProductsFile);
  echo json_encode(array($status,$srv_msg));
  return FALSE;
}


//настройки импорта
$cf_config = array();

//основная папка для загрузки файла импорта на сервер
$cf_config['load_all_category'] = (int)$_POST['load_all_category'];
$cf_config['category'] = (int)$_POST['category'];
$cf_config['directoryimage'] = $_POST['directoryimage'];
$cf_config['file'] = isset( $_FILES['loadfile'] ) ? $_FILES['loadfile'] : null;
$cf_config['fields'] = $_POST['fields'];
$cf_config['operation'] = (int)$_POST['operation'];
$cf_config['key'] =$_POST['key'];
$cf_config['is_debug'] = 0;
$cf_config['delimiter'] = $_POST['delimiter'];
$cf_config['action'] = 'import';

//создание класса для импорта продуктов
$loaderProducts = new loaderProducts($modx,$cf_config);


    switch ($cf_config['action']){
        case "import":
          $import_message = $loaderProducts->csv_import();
        break;
    }

$result = array_merge($srv_msg,$import_message);
return json_encode(array($status,$result));

?>