<?php

$srv_msg = array(); //стэк сообщений
$status = 1; //ответ во front

if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
  echo 'access denied!';
  return FALSE;
}


if ($_REQUEST['action'] == 'web/import') {
  @session_cache_limiter('public');
  define('MODX_REQP',false); //отключаем проверку прав пользователя 
}


require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.core.php';
require_once MODX_CORE_PATH.'config/'.MODX_CONFIG_KEY.'.inc.php';
require_once MODX_CONNECTORS_PATH.'index.php'; 

$importmsCorePath = $modx->getOption('importms.core_path',null,$modx->getOption('core_path').'components/importms/');


if ($_REQUEST['action'] == 'web/import') {
    $version = $modx->getVersionData();
    if (version_compare($version['full_version'],'2.1.1-pl') >= 0) {
        if ($modx->user->hasSessionContext($modx->context->get('key'))) {
            $_SERVER['HTTP_MODAUTH'] = $_SESSION["modx.{$modx->context->get('key')}.user.token"];
        } else {
            $_SESSION["modx.{$modx->context->get('key')}.user.token"] = 0;
            $_SERVER['HTTP_MODAUTH'] = 0;
        }
    } else {
        $_SERVER['HTTP_MODAUTH'] = $modx->site_id;
    }
    $_REQUEST['HTTP_MODAUTH'] = $_SERVER['HTTP_MODAUTH'];
}


$modx->switchContext('web');

$action = 'web/import';


//параметры выполнения скрипта по умолчанию
header('Content-Type:text/html;charset=utf-8');
ini_set("upload_max_filesize","15M");
ini_set("post_max_size","15M");
ini_set("max_input_time","1200"); //20 min.
ini_set('auto_detect_line_endings',1);
date_default_timezone_set('Europe/Moscow');
setlocale (LC_ALL, 'ru_RU.UTF-8');


//Путь к классу загрузки и обработки товаров
$loaderProductsFile = MODX_CORE_PATH.'components/importms/controllers/loaderproducts.class.php'; 

if (file_exists($loaderProductsFile)) {
  require $loaderProductsFile;
} else {
  $srv_msg[] = array('type_message' => 'error', 'message' => "LoaderProducts: не найден файл: ".$loaderProductsFile);
  echo json_encode(array($status,$srv_msg));
  return FALSE;
}


//запуск процессора действий (импорт/экспорт/очистка категорий и.т.д)
if(!$response = $modx->runProcessor($action, array($_POST,$FILES), 
    array('processors_path' => $importmsCorePath.'processors/',))){

    $srv_msg[] = array('type_message' => 'error', 'message' => 'Ошибка вызова процессора импорта товаров.');
    echo json_encode(array($status,$srv_msg));
    return FALSE;
}

//посылаем ответ от сервера
print_r($response->getResponse()); 

