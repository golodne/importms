<?php
echo '<select name="category" id="category" disabled="disabled">';
$list = $modx->runSnippet('pdoMenu', array(
  'parents' => 0,
  'level'=>0,
  'showUnpublished' => 1, 
  'class'=>'msCategory',
  'where' => array(
  'class_key' => 'msCategory'),
  'showUnpublished' => 1,
  'tpl' => "@INLINE <option value='[[+id]]'> [[#[[+parent]].pagetitle]]  [[+menutitle]]</option>[[+wrapper]]",
  'tplOuter' => "@INLINE [[+wrapper]]",
  'tplInner' => "@INLINE [[+wrapper]]",
  'firstClass' => "",
  'lastClass'  => ""
));
echo $list;
echo "</select>";